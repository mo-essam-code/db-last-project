<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "carrental";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
?>
<head>
    <link rel="stylesheet" href="sitestyle.css">
    </title>Reserve a Car!</title>
</head>
<body>
        <form name=myForm action="customerLogin.php" method="POST" onsubmit="return validateForm()">
            <div class="container">
                <h1>Welcome Customer!</h1>
                <hr>
                <label for="model"><b>Please choose car model:</b><br><br></label>
                <?php
                 $sql="SELECT DISTINCT model FROM car";
                    $result = $conn->query($sql);
                     $cars=array();
                    $row;
                if ($result->num_rows > 0) {

                while($row = $result->fetch_assoc())
                {
                    $cars[]=$row;
                }
                }
                 echo "<select>";
                 echo "<option value=Any>Any</option>";
                 foreach($cars as $car)
                {
                 $car=$car['model'];
                 echo "<option value='$car'>$car</option>";
                }
                echo "</select>";
                ?>


            <label for="year"><br><br><b>Please enter year of manfucture:</b><br><br></label>
            <input type="text" name="year">

            <label for="year"><br><b>Please enter Desired Color:</b><br><br></label>
            <?php
            $sql="SELECT DISTINCT color FROM car";
            $result = $conn->query($sql);
             $cars=array();
                $row;
            if ($result->num_rows > 0) {

             while($row = $result->fetch_assoc())
            {
            $cars[]=$row;
            }
             }
            echo "<select>";
            echo "<option value=Any>Any</option>";
            foreach($cars as $car)
            {
             $car=$car['color'];
                echo "<option value='$car'>$car</option>";
             }
            echo "</select>";
            ?>
            <label for="type"><br><br><b>Please enter Car Type:</b><br><br></label>
            <?php
            $sql="SELECT DISTINCT type FROM car";
            $result = $conn->query($sql);
             $cars=array();
                $row;
            if ($result->num_rows > 0) {

             while($row = $result->fetch_assoc())
            {
            $cars[]=$row;
            }
             }
            echo "<select>";
            echo "<option value=Any>Any</option>";
            foreach($cars as $car)
            {
             $car=$car['type'];
                echo "<option value='$car'>$car</option>";
             }
            echo "</select>";
            ?>
            <label for="price"><br><br><b>Please enter Price Range:</b><br><br></label>
            <label for="min_price"><b>Min Price:</b><br><br></label>
            <input type="text" name="min_price">
            <label for="max_price"><br><br><b>Max Price:</b><br><br></label>
            <input type="text" name="min_price">
            
            

            <button type="submit" class="registerbtn" >Search</button>
            <hr>
        </div>
        <div class="container signin">
            <p><a href="home.html"> HomePage</a></p>
          </div>
        </form>
</body>